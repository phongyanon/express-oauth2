const bodyParser = require('body-parser');
const express = require('express');
const config = require('config');
const port = config.get('port');
const mongoose = require('mongoose');
const OAuth2Server = require('oauth2-server');

const Request = require('oauth2-server').Request;
const Response = require('oauth2-server').Response;

// Create an Express application.
let app = express();

// Add body parser.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let oauthModel = require('./model/' + config.get('db_type'));

// connect to mongodb
if(config.get('db_type') === 'mongo'){
	let mongoUri = 'mongodb://localhost/oauth';
	mongoose.connect(mongoUri, {
		useCreateIndex: true,
		useNewUrlParser: true
	}, function(err, res){
		if(err) return console.log('Error connecting to %s', mongoUri, err);
		console.log('Connected successfully to %s', mongoUri);
	});
}

// Add OAuth server.
app.oauth = new OAuth2Server({
	model: oauthModel,
	accessTokenLifetime: 4 * 60 * 60, // 4 hours
	allowBearerTokensInQueryString: true
});

function obtainToken(req, res){
	let request = new Request(req);
	let response = new Response(res);

	return app.oauth.token(request, response)
		.then(function(token) {
			// grant_type: password => getClient -> getUser -> saveToken
			// grant_type client_credentials => getClient -> getUser -> saveToken
			// grant_type: refresh_token => getClient -> getRefreshToken ->revokeToken -> saveToken
			res.json(token);
		}).catch(function(err) {

			res.status(err.code || 500).json(err);
		});
}

function authenticateRequest(req, res, next) {

	let request = new Request(req);
	let response = new Response(res);

	return app.oauth.authenticate(request, response)
		.then(function(token) {
			// console.log('authenticate: ', token);
			req.token = token;
			next();
		}).catch(function(err) {

			res.status(err.code || 500).json(err);
		});
}


app.all('/auth/token', obtainToken);

app.get('/public', function(req, res) {
  // Does not require an access_token.
  res.send('Public area');
});

require('./routes')(app, authenticateRequest, oauthModel);

app.post('/signout', function(req, res){
	let token = {refreshToken: req.body.refresh_token};
	oauthModel.revokeToken(token).then((result) => {
		// console.log(result);
		res.send({msg: 'Successfully Sign Out!'});
	}).catch(err => console.log(err));
});

// Start listening for requests.
app.listen(port, () => {console.log(`Server running on port ${port}`)});
module.exports = app;