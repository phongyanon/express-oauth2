const config = require('config');
const mongoose = require('mongoose');
let oauthModel = require('./model/' + config.get('db_type'));

clients = [
    {
        id: 'todo',
        clientId: 'todo',
        clientSecret: 'todoSecret',
        grants: [
            'password',
            'refresh_token',
            'client_credentials'
        ],
        redirectUris: []
    },
    {
        id: 'user',
        clientId: 'user',
        clientSecret: 'userSecret',
        grants: [
            'password',
            'refresh_token',
            'client_credentials'
        ],
        redirectUris: []
    }
]

async function addClient(clients){
    // connect to mongodb
    if(config.get('db_type') === 'mongo'){
        let mongoUri = 'mongodb://localhost/oauth';
        mongoose.connect(mongoUri, {
            useCreateIndex: true,
            useNewUrlParser: true
        }, function(err, res){
            if(err) return console.log('Error connecting to %s', mongoUri, err);
            console.log('Connected successfully to %s', mongoUri);
        });
    }

    try {
        for(i in clients){
            let result = oauthModel.migrateClient(clients[i]);
        }
        console.log('add clients success.')
        // if(config.get('db_type') === 'mongo') mongoose.connection.close()
    } catch (err) {
        console.log(err);
    }
}

addClient(clients);