const Request = require('oauth2-server').Request;
const Response = require('oauth2-server').Response;

module.exports = function(app, authenticateRequest){
    app.get('/', authenticateRequest, function(req, res) {
        // console.log('token to call service', req.token);
        res.send({msg: 'Congratulations, you are in a secret area!'});
    }),
    app.get('/verifyToken', async function(req, res){
        let request = new Request(req);
        let response = new Response(res);
    
        app.oauth.authenticate(request, response)
            .then(function(token) {
                // console.log('authenticate: ', token);
                req.token = token;
                res.status(200).json({status: 'success', data: token});
            }).catch(function(err) {
                res.status(err.code || 500).json(err);
            });
    })
}