// Example
const config = require('config');
const redis_options = {
	port: config.get('redis_port')
};
const redis = require("redis"),
    client = redis.createClient(redis_options);

client.on("error", function (err) {
	console.log("Error " + err);
});

// set key value
function setKey(){
	return new Promise((resolve) => {
		client.set('hello', 'world', function(err, reply){
			console.log('setKey: ', reply);
			resolve(reply);
		});
	});
}

// get key value
function getKey(){
	return new Promise((resolve) => {
		client.get('hello', function(err, reply){
			console.log('getKey: ', reply);
			resolve(reply);
		});
	});
}

// set hash
function setHash(){
	return new Promise((resolve) => {
		client.hmset('users', ["user1", "orn", "user2", "fond", "user3", 'fond'], function(err, reply){
			console.log('setHash: ', reply);
			resolve(reply);
		});
	});
}

// get hash
function getHash(){
	return new Promise((resolve) => {
		client.hgetall('users', function(err, reply){
			console.log('getHash: ', reply);
			resolve(reply);
		});
	});
}

// flush all
function flushall(){
	return new Promise((resolve) => {
		client.flushall(function(err, reply){
			resolve(reply);
		});
	});
}

async function main(){
	try {
		let result = await flushall();
		let result2 = await setKey();
		let result3 = await getKey();
		let result4 = await setHash();
		let result5 = await getHash();
	} catch (err) {
		console.log(err);
	}
}

main();
