let mongoose = require('mongoose');

let clientSchema = mongoose.Schema({
    id: String,
    clientId: String,
    clientSecret: String,
    grants: [String],
    redirectUrls: [String]
});

let tokenSchema = mongoose.Schema({
    accessToken: String,
    accessTokenExpiresAt: Date,
    refreshToken: String,
    refreshTokenExpiresAt: Date,
    client: Object,
    user: Object    
});
tokenSchema.index({accessTokenExpiresAt: 1}, {expireAfterSeconds: 0});

let userSchema = mongoose.Schema({
    username: String,
    password: String
});

let clientModel = mongoose.model('client', clientSchema);
let tokenModel = mongoose.model('token', tokenSchema);
let userModel = mongoose.model('user', userSchema);

module.exports = {
    clientModel,
    tokenModel,
    userModel
}