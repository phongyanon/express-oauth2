const config = require('config');
const mongoose = require('mongoose');

let clientModel = require('./mongoModel').clientModel,
    tokenModel = require('./mongoModel').tokenModel,
    userModel = require('./mongoModel').userModel;

// Example client and user to database for debug
module.exports.loadExampleData = function(mockdata) {
    let client1 = new clientModel(mockdata.clients);
    let client2 = new clientModel(mockdata.confidentialClients);
    let user = new userModel(mockdata.users);

    return new Promise((resolve) => {
        client1.save(function(err, client){
            if(err) return console.log(err);
            console.log('Created client');

            user.save(function(err, user){
                if(err) return console.log(err);
                console.log('Created user');

                client2.save(function(err, client){
                    if(err) return console.log(err);
                    console.log('Created client');
                    resolve(true);
                });
            });
        });
    });
}

module.exports.truncateData = function(){
    return new Promise((resolve) => {
        clientModel.deleteMany({}, function(err, res){
            console.log('remove all client.');
            tokenModel.deleteMany({}, function(err, res){
                console.log('remove all token.');
                userModel.deleteMany({}, function(err, res){
                    console.log('remove all user.');
                    resolve(res);
                });
            });
        });
    });
}

// loadExampleData(); // run this to create debug data

// Dump the database content for debug
let dump = function(){
    clientModel.find(function(err, clients){
        if(err) return console.log(err);
        console.log('clients ', clients);
    });

    tokenModel.find(function(err, tokens){
        if(err) return console.log(err);
        console.log('tokens ', tokens);
    });

    userModel.find(function(err, users){
        if(err) return console.log(err);
        console.log('users ', users);
    });
}

/**
 * Method used by all grant types.
 */

module.exports.getAccessToken = function(token){
    return tokenModel.findOne({
        accessToken: token
    })
}

module.exports.getRefreshToken = function(bearerToken) {
    // console.log('getRefreshToken: ', bearerToken);
	return tokenModel.findOne({refreshToken: bearerToken}, function(err, token){
        if(err) return console.log(err.toString());
		return {
			clientId: token.client.id,
			expires: token.refreshTokenExpiresAt,
			refreshToken: token.refreshToken,
			userId: token.user.id
		};
	});
};

module.exports.revokeToken = function(token){
    // console.log('revokeToken: ', token);
    return new Promise((resolve) => {
        tokenModel.deleteOne({refreshToken: token.refreshToken}, function(err, refreshToken){
            if(err) resolve(err);
            // console.log(refreshToken, !!refreshToken);
            resolve(!!refreshToken);
        });
    });
}

module.exports.getClient = function(clientId, clientSecret){
    // console.log('getClient ', clientId, clientSecret);
    return clientModel.findOne({
        clientId: clientId,
        clientSecret: clientSecret
    }, function(err, client){
        if(err) console.log(err);
        let result = client.clientId === clientId && client.clientSecret === clientSecret; 
        // console.log(client, result);

        return result;
    });
}

module.exports.saveToken = function(token, client, user){
    // console.log('saveToken: ', user);
    if(user.hasOwnProperty('_id')) user.id = user._id; // only arangoDB
    token.client = {
        id: client.clientId
    };

    token.user = {
        id: user.id
    };

    let tokenInstance = new tokenModel(token);
    tokenInstance.save();
    return token;
}

/**
 * Method used only by password grant type.
 * getUser return null when not found user.
 */

module.exports.getUser = function(username, password){
    // console.log('getUser: ', username, password);
    if(config.get('config_name') === 'test'){ // only for test
        return userModel.findOne({
            username: username,
            password: password
        }, function(err, result){
            if(err) console.log(err);
            return result;    
        });
    } else {
        let userPortal = require('./userPortal');
        return userPortal.login({username: username, password: password}).then((result) => {
            if(result.status === 'success') return result.data;
            else return null;
        }).catch((err) => {console.log(err.toString())});
    }
}

/**
 * Method used only by client_credentials grant type.
 */

module.exports.getUserFromClient = function(client){
    return clientModel.findOne({
        clientId: client.clientId,
        clientSecret: client.clientSecret,
        grants: 'client_credentials'

    }, function(err, result){
        if(err) console.log(err);
        return result;    
    });
}

module.exports.migrateClient = function(client){
    let new_client = new clientModel(client);
    return new Promise((resolve) => {
        new_client.save(function(err, res){
            if(err) return console.log(err);
            console.log('migrate client');
            resolve(true);
        });
    });
}