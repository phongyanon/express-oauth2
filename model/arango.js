const config = require('config');
const arango = require('arangojs');
const aql = require('arangojs').aql;

let db = new arango.Database(config.get('arango_host'));
db.useBasicAuth(config.get('arango_user'), config.get('arango_password'));
db.useDatabase('todo');

// Example client and user to database for debug
module.exports.loadExampleData = function(mockdata) {
    let mock = mockdata.clients;
    let mock2 = mockdata.confidentialClients;
    let user = mockdata.users;

    let query = aql`INSERT {id: ${mock.id}, clientId: ${mock.clientId}, clientSecret: ${mock.clientSecret},
    grants: ${mock.grants}, redirectUris: ${mock.redirectUris}} INTO Client RETURN NEW`;
    
    let query2 = aql`INSERT {id: ${mock2.id}, clientId: ${mock2.clientId}, clientSecret: ${mock2.clientSecret},
    grants: ${mock2.grants}, redirectUris: ${mock2.redirectUris}} INTO Client RETURN NEW`;

    let query3 = aql`INSERT {username: ${user.username},
    password: ${user.password}} INTO User RETURN NEW`;

    return new Promise( async (resolve, reject) => {
        try {
            let cursor = await db.query(query);
            let result = await cursor.next();

            try {
                let cursor = await db.query(query2);
                let result = await cursor.next();
                try {
                    let cursor = await db.query(query3);
                    let result = await cursor.next();
                    
                    console.log('Created mockdata');
                    resolve(true);
                } catch (err) {
                    reject(err.toString());
                } 
            } catch (err) {
                reject(err.toString());
            } 
            
        } catch (err) {
            reject(err.toString());
        } 
    });
}

module.exports.migrateClient = function(client){
    let mock = client;
    let query = aql`INSERT {id: ${mock.id}, clientId: ${mock.clientId}, clientSecret: ${mock.clientSecret},
    grants: ${mock.grants}, redirectUris: ${mock.redirectUris}} INTO Client RETURN NEW`;

    return new Promise( async (resolve, reject) => {
        try {
            let cursor = await db.query(query);
            let result = await cursor.next();

            console.log('migrate client');
            resolve(true);
        } catch (err) {
            reject(err.toString());
        }
    });
}

module.exports.truncateData = function(){

    return new Promise( (resolve, reject) => {
        let query = aql`FOR d IN Client REMOVE d IN Client`;
        db.query(query).then(
            cursor => cursor.all()
        ).then(
            keys => {
                console.log('remove all client.');
                let query = aql`FOR d IN Token REMOVE d IN Token`;
                db.query(query).then(
                    cursor => cursor.all()
                ).then(
                    keys => {
                        console.log('remove all token.');
                        let query = aql`FOR d IN User REMOVE d IN User`;
                        db.query(query).then(
                            cursor => cursor.all()
                        ).then(
                            keys => {
                                console.log('remove all user.');                                
                                resolve(keys)
                            },
                            err => {reject(err.toString())}
                        );  
                    },
                    err => {reject(err.toString())}
                );  
            },
            err => {reject(err.toString())}
        );  
    });
}

/**
 * Method used by all grant types.
 */

module.exports.getAccessToken = function(token){
    // console.log('getAccessToken: ', token);
    let query = aql`FOR d IN Token FILTER d.accessToken == ${token} RETURN d`;
    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {
            if(keys.length > 0){
                keys = keys[0]
                keys.accessTokenExpiresAt = new Date(keys.accessTokenExpiresAt);
                keys.refreshTokenExpiresAt = new Date(keys.refreshTokenExpiresAt);
                return keys;
            } else return null;
        },
        err => {console.log(err.toString())}
    );
}

module.exports.getRefreshToken = function(bearerToken) {
    // console.log('getRefreshToken: ', bearerToken);
    let query = aql`FOR d IN Token FILTER d.refreshToken == ${bearerToken} RETURN d`;
    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {            
            if(keys.length > 0) {
                keys = keys[0];
                keys.refreshTokenExpiresAt = new Date(keys.refreshTokenExpiresAt);
                let result = {
                    client: keys.client,
                    expires: keys.refreshTokenExpiresAt,
                    refreshToken: keys.refreshToken,
                    user: keys.user
                };
                return result
            } else return null;
        },
        err => {console.log(err.toString())}
    );
};

module.exports.revokeToken = function(token){
    // console.log('revokeToken: ', token);
    let query = aql`FOR d IN Token FILTER d.refreshToken == ${token.refreshToken} 
        REMOVE d IN Token`;

    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {
            if(keys) return true;
            else return false;
        },
        err => {console.log(err.toString())}
    );
}

module.exports.getClient = function(clientId, clientSecret){
    // console.log('getClient ', clientId, clientSecret);
    let query = aql`FOR d IN Client FILTER 
        d.clientId == ${clientId} && d.clientSecret == ${clientSecret} LIMIT 1 RETURN d `;

    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {
            // let result = keys[0].clientId === clientId && keys[0].clientSecret === clientSecret;
            let client = keys[0];
            let result = {
                id: client.id,
                redirectUris: client.redirectUris,
                grants: client.grants
            };
            return result;
        },
        err => {console.log(err.toString())}
    );
}

module.exports.saveToken = function(token, client, user){
    // console.log('saveToken: ', user, client, token);
    if(user.hasOwnProperty('_id')) user.id = user._id; // only arangoDB

    token.client = {
        id: client.id
    };

    token.user = {
        id: user.id
    };

    token.accessTokenExpiresAt = new Date(token.accessTokenExpiresAt);
    token.refreshTokenExpiresAt = new Date(token.refreshTokenExpiresAt);

    let query = aql`INSERT {
        refreshToken: ${token.refreshToken}, 
        refreshTokenExpiresAt: ${token.refreshTokenExpiresAt},
        accessToken: ${token.accessToken},
        accessTokenExpiresAt: ${token.accessTokenExpiresAt},
        client: ${token.client},
        user: ${token.user}
    } INTO Token RETURN NEW`;

    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {return token},
        err => {console.log(err.toString())}
    );
}

/**
 * Method used only by password grant type.
 */

module.exports.getUser = function(username, password){
    // console.log('getUser: ', username, password);
    if(config.get('config_name') === 'test'){ // only for test
        let query = aql`FOR d IN User FILTER 
        d.username == ${username} && d.password == ${password} RETURN d`;

        return db.query(query).then(
            cursor => cursor.all()
        ).then(
            keys => {
                if (keys) return keys[0];
                else return null;
            },
            err => {console.log(err.toString())}
        );
    } else {
        let userPortal = require('./userPortal');
        return userPortal.login({username: username, password: password}).then((result) => {
            if(result.status === 'success') return result.data;
            else return null;
        }).catch((err) => {console.log(err.toString())});
    }
}

/**
 * Method used only by client_credentials grant type.
 */

module.exports.getUserFromClient = function(client){
    let query = aql`FOR d IN Client FILTER 
        d.clientId == ${client.clientId} && d.clientSecret == ${client.clientSecret} && 
        d.grants: 'client_credentials'
        RETURN d`;

    return db.query(query).then(
        cursor => cursor.all()
    ).then(
        keys => {return keys[0];},
        err => {console.log(err.toString())}
    );
}