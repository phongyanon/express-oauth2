const config = require('config');
const redis_options = {
	port: config.get('redis_port'),
	password: config.get('redis_password')
};
const redis = require("redis"),
		db = redis.createClient(redis_options);


module.exports.loadExampleData = function(mockdata) {
  	let mock = mockdata.clients;
  	let mock2 = mockdata.confidentialClients;
	let user = mockdata.users;

	mock.grants = mock.grants.toString();
	mock2.grants = mock2.grants.toString();
	mock.redirectUris = mock.redirectUris.toString();
	mock2.redirectUris = mock2.redirectUris.toString();

	return new Promise((resolve) => {
		db.multi()
		.hmset(`users:${user.username}`, user, function(err, res){
			if(err) resolve(err.toString());
			console.log('create mock user');
		})
		.hmset(`clients:${mock.clientId}`, mock, function(err, res){
			if(err) resolve(err.toString());
			console.log('create mock client');
		})
		.hmset(`clients:${mock2.clientId}`, mock2, function(err, res){
			if(err) resolve(err.toString());
			console.log('create mock client2');
		})
		.exec(function (errs) {
			if (errs) console.error(errs[0].message);
			console.log('successfully create mock data');
			resolve(true);
		});
	});
}

module.exports.truncateData = function(){
	return new Promise((resolve) => {
		db.flushall(function(err, reply){
			console.log('Truncate Redis')
			resolve(reply);
		});
	});
}

/**
 * Get access token.
 */

module.exports.getAccessToken = function(bearerToken, callback) {
	// console.log('getAccessToken: ', bearerToken);
	db.hgetall(`tokens:${bearerToken}`, function(err, token){
		if(err) resolve(err.toString());
		if(token === null){
			callback(null, null);
		} else {
			token.accessTokenExpiresAt = new Date(token.accessTokenExpiresAt);
			token.refreshTokenExpiresAt = new Date(token.refreshTokenExpiresAt);
			token.client = JSON.parse(token.client);
			token.user = JSON.parse(token.user);
	
			callback(null, token);
		}
	});
};

/**
 * Get client.
 */

module.exports.getClient = function(clientId, clientSecret, callback) { // only work with callback style
	// console.log('getClient ', clientId, clientSecret);
	db.hgetall(`clients:${clientId}`, function(err, client){
		if(err) console.log(err.toString());
		if (client.redirectUris === '') client.redirectUris = [];
		let result = {
			id: client.id,
			redirectUris: client.redirectUris,
			grants: client.grants.split(',')
		};
		callback(null, result);
	});
};

module.exports.getUserFromClient = function(client){
	return db.hgetall(`clients:${client.clientId}`, function(err, client){
		if(err) console.log(err.toString());
		if(client.redirectUris === '') client.redirectUris = [];
		client.grants = client.grants.split(',');
		return client;
	});
}

/**
 * Get refresh token.
 */

module.exports.getRefreshToken = function(bearerToken, callback) {
	db.hgetall(`tokens:${bearerToken}`, function(err, token){
		if(err) console.log(err.toString());

		token.accessTokenExpiresAt = new Date(token.accessTokenExpiresAt);
		token.refreshTokenExpiresAt = new Date(token.refreshTokenExpiresAt);
		token.client = JSON.parse(token.client);
		token.user = JSON.parse(token.user);

		callback(null, token);
	});
};

// add user
module.exports.addUser = function(username, password, email){
	let data = {
		username: username,
		password: password,
		email: email
	}
	// console.log('addUser: ', data);
	return db.hmset(`users:${username}`, data, function(err, res){
		if(err) console.log(err.toString());
		console.log('success');
		return(res);
	});
}

/**
 * Get user.
 */

module.exports.getUser = function(username, password, callback) { // use callback style only
	// console.log('getUser: ', username, password);
    if(config.get('config_name') === 'test'){ // only for test
		db.hgetall(`users:${username}`, function(err, user){
			if(err) console.log(err.toString());
			if (!user || password !== user.password) callback(null, null);
			else callback(null, user);
		});
    } else {
		let userPortal = require('./userPortal');
        userPortal.login({username: username, password: password}).then((result) => {
            if(result.status === 'success') callback(null, result.data);
            else callback(null, null);
        }).catch((err) => {console.log(err.toString())});
    }
};

/**
 * Save token.
 */

module.exports.saveToken = function(token, client, user, callback) {
	// console.log('saveToken: ', token, client, user);
	let data = {
		accessToken: token.accessToken,
		accessTokenExpiresAt: token.accessTokenExpiresAt,
		client: `{"id":"${client.id}"}`,
		refreshToken: token.refreshToken,
		refreshTokenExpiresAt: token.refreshTokenExpiresAt,
		user: `{"id":"${user.username}"}`
	};
	
	db.hmset(`tokens:${token.accessToken}`, data, function(err, res){
		if(err) console.log('failed to save accessToken!');
		db.hmset(`tokens:${token.refreshToken}`, data, function(err, res){
			if(err) console.log('failed to save refreshToken');
			data.client = JSON.parse(data.client);
			data.user = JSON.parse(data.user);
			callback(null, data);
		});
	})
};

module.exports.revokeToken = function(token){
	// console.log('revokeToken: ', token);
	return new Promise((resolve) => {
		let del_token;
		
		db.hgetall(`tokens:${token.refreshToken}`, function(err, token){
			del_token = token;
			db.del(`tokens:${del_token.accessToken}`, function(err, reply){
				db.del(`tokens:${del_token.refreshToken}`, function(err, reply){
					if(reply === 1) resolve(true);
					else resolve(false);
				});
			})
		})
	}); 
}

module.exports.migrateClient = function(client){
    let mock = client;
    return new Promise((resolve) => {
		db.hmset(`clients:${mock.clientId}`, mock, function(err, res){
			if(err) resolve(err.toString());
			console.log('create mock client');
			resolve(true);
		})
    });
}

// module.exports.saveAuthorizationCode = function(code, client, user){
//     // imaginary DB queries
//     console.log('saveAuthorizationCode: ', code);
//     let authCode = {
//         authorization_code: code.authorizationCode,
//         expires_at: code.expiresAt,
//         redirect_uri: code.redirectUri,
//         scope: code.scope,
//         client_id: client.id,
//         user_id: user.id
//     };
//     return {
//         authorizationCode: 'authorizationCode.authorization_code',
//         expiresAt: new Date('2019-06-20'),
//         redirectUri: 'authorizationCode.redirect_uri',
//         scope: 'authorizationCode.scope',
//         client: {id: 1},
//         user: {id: 2}
//     };
// }